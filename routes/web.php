<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/table', 'TaskController@Return');
Route::post('/table/create', 'TaskController@Create')->name('create');
Route::delete('/table/delete/{Task}', 'TaskController@Delete' )->name('delete');

Route::get('/', function () {
    return view('welcome');
});
