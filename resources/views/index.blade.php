<!doctype html>
<html lang="en">
    <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template 
    <link href="grid.css" rel="stylesheet">
    -->
  </head>




  <body class="py-4">
    <div class="main">
    <div class="container">

  <h1>Tasks</h1>

  <div class="row mb-4">
    <div class="col-5 themed-grid-col">
    <form action="{{ route('create') }}" method="POST">
                        @csrf
                        <div class="form-group card-body">
                        <p class="card-text">Task text:</p>
                        <textarea type="text" class="form-control" name="text" id="DataText" placeholder="Enter text" cols="120" rows="3"></textarea>
                        </div>
                        <button type="submit" class=" ml-3 btn btn-primary">Create</button>
    </form>
    </div>
  </div>
    </div>
  </div>

  <table class="table table-striped">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Task</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody class="tbody-light">
    @foreach ($tasks as $item)
    <tr>
      <th scope="row">{{ $loop->index + 1 }}</th>
      <td>{{ $item->task }}</td>
      <td><form action="{{ route('delete', ['id' => $item->id]) }}" method="POST">
            @csrf
            {{ method_field("DELETE") }}
            <button type="submit" class="btn btn-danger">Delete</button>
        </form></td>
    </tr>
    @endforeach
  </tbody>
</table>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    </body>
</html>